﻿using System;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] private GameObject destroyFX;
    [SerializeField] private Transform parent;

    [SerializeField] private int scorePerHit = 12;
    [SerializeField] private int maxHits = 5;
    ScoreBoard scoreBoard;

    private void Start()
    {
        AddNonTriggerBoxCollider();
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    private void AddNonTriggerBoxCollider()
    {
        Collider sphereCollider = gameObject.AddComponent<SphereCollider>();
        sphereCollider.isTrigger = false;
    }

    private void OnParticleCollision(GameObject other)
    {
        maxHits--;
        scoreBoard.ScoreHit(scorePerHit);

        if(maxHits<=0)
        {
            KillEnemy();
        }
    }

    private void KillEnemy()
    {
        GameObject fx = Instantiate(destroyFX, transform.position, Quaternion.identity);

        fx.transform.parent = parent;

        Destroy(gameObject);
    }
}
