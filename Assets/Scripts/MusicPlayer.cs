﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour {

    // Use this for initialization
    private void Awake()
    {
        int numMP = FindObjectsOfType<MusicPlayer>().Length;
        print("number of music player in this scene: [ " + numMP + " ]");
        if(numMP > 1)
        {
        Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
    private void Start()
    {
        Invoke("loadNextScene", 4f);
    }
     void loadNextScene()
    {
        SceneManager.LoadScene(1);
    }
}
