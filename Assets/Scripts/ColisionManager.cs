﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColisionManager : MonoBehaviour {

    [Tooltip("inSecond")]
    [SerializeField] private float howLong = 1f;
    [Tooltip("FX prefab on player")]
    [SerializeField] private GameObject deathFX; 

    private void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();
        deathFX.SetActive(true);
        Invoke("LoadThisScene", 2f);
    }

    private void StartDeathSequence()
    {
        print("pleyer dying");
        SendMessage("OnPlayerDeath");
    }
    private void LoadThisScene()
    {
        SceneManager.LoadScene(1);
    }
}
